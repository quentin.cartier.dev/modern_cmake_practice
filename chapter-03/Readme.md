# Chapter 3

## Partition

*Design principle* : ***separation of concerns***
- Break the code into chunks, grouping code with closely related functionality, while decoupling other pieces of code to create strong boundaries.
 - *include()* allows CMake to execute code from an external file. It doesn't introduce any scopes or isolations that are not defined within the file.

 ## Scoped subdirectories

 - **add_library** : To produce a globally visible target.

 ## Project structure

 As a project grows, it becomes harder and harder to find things in it.
 Rules:
 - Easy to navigate and extend
 - Self-contained. Project-specific files should be in the project directory and nowwhere else
 - Abstraction hierarchy should be expressed through executables and binaries.

 No aggreed unique solution. Here a suggestion:

```mermaid
graph LR
A1[Project] --> B1("cmake")
A1 --> B2("src")
A1 --> B3("doc")
A1 --> B4("extern")
A1 --> B5("test")

B1 --> C1a("include")
B1 --> C1b("module")
B1 --> C1c("script")

B2 --> C2a("app1")
B2 --> C2b("app2")
B2 --> C2c("lib1")
B2 --> C2d("lib2")


```

- *cmake* : Includes macros and functions, find_modules, and one-off scripts.
- *src* : Stroe the source of binaries and libraries.
- *doc* : Used for building the doc.
- *extern* : Configuration for the external projects we are building from source.
- *test* : Contains code for automated tests.
#include <iostream>
#include "lib1.h"

int main() {
    Lib1 lib1;
    std::cout << "Lib1 call : " << lib1.method() << std::endl;
    return 0;
}
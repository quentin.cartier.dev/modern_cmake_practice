cmake_minimum_required(VERSION 3.18.4)
message("*** Examining filesystem in CMake ***")

# Defining a macro

macro(MyMacro MyParam)
    set(MyVar ${MyParam})
    message("MyMacro called with parameter : ${MyParam}")
endmacro()


set(MyVar "Initial Value")
message("MyVar (before MyMacro call) : ${MyVar}")
MyMacro("I'm the new value for MyVar!")
message("MyVar (after MyMacro call) : ${MyVar}")


# Funtion

function(MyFunctionName MyFirstParam)
    message("Function : ${CMAKE_CURRENT_FUNCTION}")
    message("File : ${CMAKE_CURRENT_FUNCTION_LIST_FILE}")
    message("MyFirstParam : ${MyFirstParam}")
    message("ARGV0 : ${ARGV0}, ARGV1 : ${ARGV1}, ARGC : ${ARGC}")
endfunction()

MyFunctionName("OneParam" "TwoParam")

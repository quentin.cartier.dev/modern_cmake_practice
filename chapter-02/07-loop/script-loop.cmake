cmake_minimum_required(VERSION 3.18.4)
message("*** Examining filesystem in CMake ***")

set(MyCnt 1)
while(MyCnt LESS_EQUAL 5)
    message("MyCnt = ${MyCnt}")
    MATH(EXPR MyCnt "${MyCnt}+1")
endwhile()

set(MyList 1 2 3)
foreach(LoopVar IN LISTS MyList ITEMS "Yoohoo" "Another Element in the loop")
    message("${LoopVar}")
endforeach()

#Zip list ( since 3.17
if(${CMAKE_VERSION} VERSION_GREATER_EQUAL 3.17.0)
    message("OK")
    set(MyListA "one" "two" "three")
    set(MyListB "1;2;3")
    message("\t Zip lists:")
    foreach(MyNum IN ZIP_LISTS MyListA MyListB)
        message("\t${MyNum_0} and ${MyNum_1}")
    endforeach()
endif()
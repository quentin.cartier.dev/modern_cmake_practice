cmake_minimum_required(VERSION 3.18.4)
message("*** Examining filesystem in CMake ***")
message("\n\t--> EXISTS")
message("\t Current source directory: ${CMAKE_CURRENT_SOURCE_DIR}")
message("\t Current source directory: ${CMAKE_SOURCE_DIR}")

if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/script-filesystem.cmake")
    message("\t\t File ${CMAKE_CURRENT_SOURCE_DIR}/script-filesystem.cmake EXISTS !")
else()
message("\t\t File ${CMAKE_CURRENT_SOURCE_DIR}/script-filesystem.cmake DOES NOT exist")
endif()

if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/script-comparing.cmake")
    message("\t\t File ${CMAKE_CURRENT_SOURCE_DIR}/script-comparing.cmake EXISTS TOO !")
else()
    message("\t\t File ${CMAKE_CURRENT_SOURCE_DIR}/script-comparing.cmake DOES NOT exist")
endif()

# Which one is newer ? 
if( "${CMAKE_CURRENT_SOURCE_DIR}/script-filesystem.cmake" IS_NEWER_THAN "${CMAKE_CURRENT_SOURCE_DIR}/script-comparing.cmake")
    message("\t\t File script-filesystem.cmake IS NEWER !")
else()
message("\t\t File script-comparing.cmake IS NEWER !")
endif()

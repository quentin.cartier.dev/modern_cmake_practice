cmake_minimum_required(VERSION 3.18.4)
message("*** Comparing values in CMake ***")
message("\n\t--> Comparing versions:")


message("My version CMake : ${CMAKE_VERSION}")

if("${CMAKE_VERSION}" VERSION_LESS_EQUAL 3.20.0)
    message("Version <= than 3.20.0")
else() 
    message("Version > than 3.20.0")
endif()

if("${CMAKE_VERSION}" VERSION_LESS_EQUAL 3.16.5)
    message("Version <= than 3.16.5")
else()
    message("Version > than 3.16.5")
endif()

message("\n\t--> Comparing strings:")
set(FOO "MyLittlePony")
if("MyLittlePony" STREQUAL "${FOO}")
    message("I'm watching a nice cartoon, named ${FOO}")
endif()

message("\n\t--> Using regex and MATCH operator:")
set(MyMatchVar "String_0 StrinTeg_1 Test_2")
string(REGEX MATCH "[T][a-z]+[_]" NewVar "${MyMatchVar}")
message(STATUS "My new var : ${NewVar}")
message(STATUS "My new var : ${CMAKE_MATCH_0}")

if("ABCDABC,DABCD" MATCHES "([A].*),([D].*)")
    foreach(i RANGE 0 ${CMAKE_MATCH_COUNT})
        message("\t\tGroup ${i} MATCHES : ${CMAKE_MATCH_${i}}")
    endforeach()
endif()



message("\n\t--> Using FIND:")
set(var "String_0 String_1 String_2")

string(FIND ${var} " " wsloc)
message(STATUS "position of first whitespace character: ${wsloc}")


message("\n\t--> Check IN_LIST:")
set(MyList A B C D E)
# Note that the second argument below is expected to be a list of variables (not a list)
if( "B" IN_LIST MyList )
    message("\n\t--> Yes, B is in ${MyList}")
endif()
cmake_minimum_required(VERSION 3.18.4)
message("*** Conditional blocks in CMake ***")

set(MyVar1 FALSE)
set(MyVar2 "MyVar1")
# It will first evaluate MyVar2 to MyVar1, and then evaluate to FALSE
# Due to legacy reasons
if(${MyVar2})
    message("MyVar2 is true. MyVar2 : ${MyVar2}")
else()
    message("MyVar2 is false. MyVar2 : ${MyVar2}")
endif()

set(MyVar1 TRUE)
if(${MyVar2})
    message("Now MyVar2 is true!.")
endif()

set(FOO BAR)

# Avoiding behavior of unquoted arguments by wrapping it

set(FOO "FOO")
if(FOO)
    message("FOO is defined, and equals to ${FOO}")
endif()

if("${FOO}")
    message("FOO is true")
else()
    message("FOO is false")
endif()

# We can explicitely ask if the variable is defined without using this confusing syntax

if(DEFINED FOO) 
    message("Yes, FOO is defined")
endif()
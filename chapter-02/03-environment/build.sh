#!/bin/bash
export myenv=thirst
echo myenv is now $myenv
cmake -B build
cd build
export myenv=second
echo myenv is now $myenv
cmake --build .
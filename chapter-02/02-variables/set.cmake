set(MyStrVar1 "Hello1")
set([[My Str Var 2]] "Hello2") # Remember we can use [{=}+] to avoid interpretation
set("My Str Var 3" "Hello3")

set([[My String2]] "Text2")
message(${My\ String2})

message(${MyStrVar1})
message(${My\ Str\ Var\ 2}) # We have to escape the whitespace with \
message(${My\ Str\ Var\ 3})

# It is highly not recommended to use whitespace in variable names
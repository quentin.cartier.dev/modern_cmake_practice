cmake_minimum_required(VERSION 3.18.4)
message("*** Lists in CMake ***")

# CMake  concatenates all elements into a string, usign a semicolon (;)

set(myList "a;b;c;d;e;f")
set(myList2 a b c d e f)

message("List (1) => ${myList}")
message("List (2) => ${myList2}")
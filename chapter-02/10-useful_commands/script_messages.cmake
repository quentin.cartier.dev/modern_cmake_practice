cmake_minimum_required(VERSION 3.18.4)
message("*** Messages in cmake***")

function(foo)
    list(APPEND CMAKE_MESSAGE_CONTEXT "foo")
    # Custom indent inside this function scope
    list(APPEND CMAKE_MESSAGE_INDENT "  ")
    message("foo message")
endfunction()

list(APPEND CMAKE_MESSAGE_CONTEXT "top")
message("Before 'foo'")
foo()

message("After 'foo'")

message(TRACE "You need to use --log-level=TRACE in command line to see this trace message")
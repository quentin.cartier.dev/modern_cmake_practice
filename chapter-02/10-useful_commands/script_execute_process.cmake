cmake_minimum_required(VERSION 3.18.4)

set(MyCommand "ls")
set(MyArg "-a")

execute_process(COMMAND ${MyCommand} ${MyArg} OUTPUT_VARIABLE res )

message("** Result of command \"${MyCommand} ${MyArg}\": \n ${res}")
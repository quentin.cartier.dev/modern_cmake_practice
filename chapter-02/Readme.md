# Readme

## Arguments

Commands: 
```
cmake -P 01-arguments/bracket.cmake
cmake -P 01-arguments/quoted.cmake
cmake -P 01-arguments/unquoted.cmake
```

### Comments

There is two kin of comments : 
- single-line
- bracket (multiline)


Unlike C++, bracket comment **can be nested**.
```
#signle-line comments start with a hash sign "#"
# they can be placed on an empty line
message("Hi"); # or after a command like here

# [=[
bracket comment
    #[[
        nested bracket comment
    ]]
#]=]
```

#### Nested comments
Start with an opening squre bracket "[" + any number of equal sing "=", and another bracket "[".

### Command invocations
```
message("hello world")
```
- Command are not case-sensitive.
- Convention to use **snake_case**
- Everything betwenn the parentheses is interpreted as a an argument for that command (Thus, can't provide another command as an argument)

#### Category of command
1. Scripting commands: Always available, they change the state of command processor, access variables, and affect other commands and the env.
2. Project commands: Available in projects, and manipulate the project state and build targets.
3. CTest commands: Available in CTest scripts.

## Command Arguments

Many commands : Require whitespace separated arguments .
- The only data type recognized by CMake is string.
- CMake wil evaluate every argument to a static string, then pass then into command.
- Evaluating = string interpolation ( substituyng parts of a string with another value)
** Can be replacing escape sequences, expanding variable references, or unpacking lists.

### Type of arguments
1. Bracket arguments
2. Quoted arguments
3. Unquoted arguments

#### Bracket argument
Use same syntax as multiline bracket, but can't be nested.
Content inside brackets is seen as a single argument.
```
message([==[
    things inside is a single argument
]==])
```

#### Quoted argument
- Can span multiple lines
- Will interpolate variable reference
- To insert a variable reference, wrap the name of the variable in the token as ${name}
```
message("Message spanned 
    multiline with variable interpolatted ${CMAKE_VERSION}"))
```

#### Unquoted argument
- Both evalute escape sequences and variable references.
- Wil use ";" to separate arguments ( to avoid this, escape \;). This is how CMake manages lists.
- Cannot contain unescaped '"', '#', and backslasheds '\'.
- Parentheses () allowed only by pairs.
```
argument\ ent\;1 # A single argument
arg;ume nts # Three arguments
```

## How to *pass arguments script* ?
In  the command line, you can pass aguments to script after a *--* token.
```
cmake -P <my_script> -- <myargs>
```


# Variables
## Three category of variables:
1. Normal
2. Cache
3. Environment

- Run on specific scope.
- The **${}** is used to reference **normal** and **cache** variables.
- The **$ENV{}** syntax is used to reference **environment** variables.
0 THe **$CACHE{}** syntax is used to reference **cache** varables.


## Basics about variables: 
- Case -sensitive, can include most character
- Stored internally as strings, even if some commands can interpret them as values of other data types ( lists)
- Basic Commands to manipulate variables are **set()** and **unset()**. Some other commands can affect varaibles such **string()** and **list()**.

### Example
This example actually shows syntax error, related to the use of "\" in reference variable.
```
cmake -P ./02-variables/set.cmake
```

Error message (part of it): 

```
CMake Warning (dev) at 02-variables/set.cmake:10 (message):
  Syntax error in cmake code at

    /home/daoliangshu/Workplace/practice_cmake/chapter-02/02-variables/set.cmake:10

  when parsing string

    ${My\ Str\ Var\ 3}

  syntax error, unexpected cal_SYMBOL, expecting } (6)

  Policy CMP0010 is not set: Bad variable reference syntax is an error.  Run
  "cmake --help-policy CMP0010" for policy details.  Use the cmake_policy
  command to set the policy and suppress this warning.
```


## Variable references
```
set(myVar "Value")
message(${myVar})
```
On evaluation, CMake will traverse the scope stack, and replace ${myVar} by the value "Value".
- If myVar is not defined, it will display empty string. It never show error messages.
- Interpolation is performed in an **inside-out fashion**. That means , that is we have something like : 
```
message(${A${B}})
```
It will first try to evaluate B, then A with evaluated  value of B.


# Using Environment variables

## 03-Environment

The value set during the configuration is persisted to the generated buildsystem

```
# Changing the value for the configuration stage
export myenv=first # This value is persisting
cmake -B build .
cd build

# Changing the value for the build phase (won't have any effect)
export myenv=second # Not taken into account
cmake --build .
```

In /build/EchoEnv.dir/build.make, we can see this line: 
```
CMakeFiles/EchoEnv:
	echo myenv\ in\ build\ is first
```

That is related to the custom target in the CMakeLists.txt: 
```
add_custom_target(EchoEnv ALL COMMAND echo "myenv in build is" $ENV{myenv})
```

# Using the cache variables

They're are persistent variables stored in the CMakeCache.txt in the build tree.
- Contain information **during the project configuration stage**
- Info from the system (path to compilers, linkers, tools)
- Info from user (from GUI)
- Exists **only in project** ( not in scripts)

```
set(<variable> <value> CACHE <type> <docstring> [FORCE])
```
**Types** possible:
- BOOL: on/off value. Will be shown as checkbox in GUI.
- FILEPATH
- PATH
- STRING
- INTERNAL: A line of text

**docstring** :
*A labl that will be displayed by the GUI next to the field*

**FORCE** : If a value already exist in the cache, FORCE will make the new value persisting. If not FORCE, the the value returns to the one before across execution.


# Variable scopes in CMake
CMake has 2 scopes:
- Function scope
- Directory scope
When a nested scope is created, CMake fills it with copies from the current scope. When leaving the scope , copies are deleted, and parent scope is restored.

## Implications

- unset() a variable created in parent scope unset the variable for current scope, but restored when leaving the nested scope (going back to parent scope)

- Referencing a varaible : When we try to get a variable, CMake first try to get it from current scope. If not found, it will check in cache.

### How to change a value in the parent scope ?
Using *PARENT_SCOPE* flag.
```
set(MyVar "New Value" PARENT_SCOPE)
unset(MyEnv PARENT_SCOPE)
```

- Can only access scope n - 1, now above.

# Using lists

Script:
```
set(myList "a;b;c;d;e;f")
set(myList2 a b c d e f)
message("List (1) => ${myList}")
message("List (2) => ${myList2}")
```

Result: 
```
*** Lists in CMake ***
List (1) => a;b;c;d;e;f
List (2) => a;b;c;d;e;f
```

In the second list, we use set and give it several arguments (a, b, c ...), that are added to the list. It is equivalent to the first variable.

# Conditional blocks
Only if() is supported in CMake
```
if(<condition>)
  <commands>
elseif(<other_condition>)
  <commands>
else()
  <commands>
endif()
```

## Logical operation for \<condition\> 
- **NOT** \<condition\>
- \<condition\> **AND** \<condition\>
- \<condition\> **OR** \<condition\>
Nesting conditions is possible: 
- \<condition1\> OR (\<condition2\> AND \<condition3\>)

## Evaluation of string and variable

For legacy reasons, CMake will try to evaluate unquoted variable as if it was variable reference.
In the example below,MyVar2 will be evaluated to MyVar1, but CMake will try to evaluate MyVar1 even if unquoted, which lead to TRUE.
```
set(MyVar1 TRUE)
set(MyVar2 "MyVar1")
if(${MyVar2}) 
  [...] # TRUE
endif()
```

# TRUE or FALSE ?
String are **true** if equals to:
- ON, Y, YES, or TRUE, or
- A non-zero number

**BUT** exception for *unquoted* variable reference.
For example, 
```
set(FOO BAR)
if(F00) # Do not meet condition to be true according to rules above, but is still evaluated as true! 
```
For *unquoted* variable refernece, it is false if:
- OFF, NO, FALSE, N, IGNORE, NOTFOUND or
- A string ending with -NOTFOUND
- An empty string
- Zero
Simply said, it will be evaluated to FALSE, if the variable is undefined
```
# FOO is not defined before)
if(FOO) # Evaluated to false
```

## Avoiding the confusion

We can ask explicitly if a variable is defined
```
if(DEFINED MyVar)
if(DEFINED CACHE{MyVarInCache})
if(DEFINED ENV{MyENvVar})
```


# Comparing values

```
if(1 LESS 2)
if(3.18.4 VERSION_LESS_EQUL 3.20.0)
if("A" STREQUAL "${MyVar}")
```

- For strings comparisons, we add STR prefix (no underscore)
- For version comparisons, VERSION_ (with underscore)

## POSIX regex

Using **MATCHES** operator
```
<VARIABLE|STRING> MATCHES <regex>
```

Any matching groups are captured in **CMAKE_MATCH_<n>"variables

## Examining filesystem

- EXISTS
- IS_NEWER_THAN
- IS_DIRECTORY
- IS_SYMLINK
- IS_ABSOLUTE

# Loops

- TODO

## While
```
while(<continue>)
  <commands>
endwhile()
```
- break()
- continue()

## Foreach
```
foreach(<loop_var> RANGE <max>)
  <commands>
endforeach()

foreach(<loop_var> RANGE <min> <max> [<step>])

foreach(<loop_var> IN [LISTS <lists>] [ITEM <items>])
```
# Command definitions



## Macro


```
macro(<name> [<argument>])
  <commands>
endmacro()
```

## Functions

```
function(<name> [<argument>])
  <commands>
endfunction()
```

## Difference between *macros* and *function* 

A *macro* works more like find-and-replace instruction than an actual subroutine.
+ Do not create a separate entry in call stack. So, if we return inside a macro, it actually return 1 level upper. Macros works on the scope of the caller.
+ A function creates a separate scope for local variables.


## Variables set by CMake in functions :
- CMAKE_CURRENT_FUNCTION
- CMAKE_CURRENT_FUNCTION_LIST_DIR
- CMAKE_CURRENT_FUNCTION_LIST_FILE
- CMAKE_CURRENT_FUNCTION_LIST_LINE


## Procedural paradigm in CMake

If we would write CMake in the same way as a program in C++, we would defined 3 commands that may call defined commands of their on:
```mermaid
graph LR
A[CMakeLists.txt] --> |1| B1("setup_first_target()")
A --> |2| B2("setup_second_target()")
A --> |3| B3("setup_tests()")
B1 --> C1("pull_shared_protobuff()")
B2 --> C2("calculate_version()")

```

But in CMake, you are required to provide command definitions you're planning to sue ahead of time.



## Naming conventions 

- Follow a consistent naming style (free)
- Upper case for CMake commands

# Useful commands

## message()

We can provide a **MODE** argument to message() to customize the style of the output.
MODES:
- FATAL_ERROR
- SEND_EROR
- WARNING
- AUTHOR_WARNING
- DEPRECATION
- NOTICE
- **STATUS** : This continues processing and is recommended for main messages for users.
- VERBOSE
- DEBUG
- **TRACE** : Continues processing and is recommentded to print messages during project development.

*STATUS* is the default log level.



We can enable a log-level, with the command-line flag:
```
cmake --log-context
```

Messages will be decorted with do-separated context, and stored in *CMAKE_MESSAGE_CONTEXT* list.

```
cd /chapter-02/10-useful_commands
cmake -P script_messages.cmake --log-level=TRACE

```


## include() and include_guard()

### include()
- To partition CMake code into separate files to keep things ordered and well separate.

```
include (<file|module> [OPTIONAL] [RESULT_VARIABLE <var>])
```
- *OPTIONAL* : CMake raise an error if a file doesn't exist, unless we specify this flag.
- When running in *script*:
Relative paths are from the current working directory.
We can force to search relative to the script itself, we need to provide absolute path:
```
include("${CMAKE_CURRENT_LIST_DIR}/<filename>.cmake")
```

### include_guard()
- Restrict the includes files to be included once.
To avoid side effects.

This line need to be add at the top of the included file:
```
include_guard([DIRECTORY|GLOBAL])
```
- *DIRECTORY* : apply the protection within the current directory and below.
- *GLOBAL* : apply the protection to the whole build.

```
cd /chapter-02/10-useful_commands
cmake -P script_include.cmake --log-level=TRACE
```

## file() 
**file()** let you read, write, transfer files and work with the filesystem.
- System-independant manner

```
file(READ <filename> <out-var> [...])
file({WRITE | APPEND} <filename> <content> ...)
file(DOWNLOAD <url> [<file>] [...])
```

## execute_process()

*exectue_process* is used to run other processes and collect their outpur.
- Can be use in scripts, and also in projects during the configuration stage.

```
execute_process(COMMAND <cmd1> [<argument>]... [OPTIONS])
```
- *TIMEOUT* option (in seconds)
- Can chain commands, and pass out of one to the next by providing ```COMMAND <cmd> <arguments>``` more than once.
- *RESULTS_VARIABLE* : collects the exit codes of all tasks in this list.
- *OUTPUT_VARIABLE* and *ERROR_VARIABLE* : To collect output 
- When using a command, you should make sure that this command is available in the platforms you target.
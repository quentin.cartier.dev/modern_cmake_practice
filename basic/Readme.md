# Readme

## Using docker

```
docker pull swidzinski/cmake:examples
docker run -it swidzinski/cmake:examples

```

## CMake tools

Five executables
```
cmake # Main exec that config, generates, and builds projects.
ctest # Test driver prog. to run and report test results
cpack # Packagin prog. and gen. installers and source packages
cmake-gui # Graph wrapper around cmake
ccmake # Console-based GUI wrapper around cmake
```

# Generating a project buildsystem

```
cmake [<options>] -S <path-to-source> -B <path-to-build> # Recommended
cmake [<options>] <path-to-source>
cmake [<options>] <path-to-existing-build>
```

## Options for generators

```
cmake -G <generator-name> <path-to-source>
```

### Which build tool ?
Usually, we should not care. CMake does support multiple native buildsystems on many platforms.
- Unless you have a few of them installed at the same time, CMake will correctly select it for you. It can be overriden byt **CMAKE_GENERATOR** env variable.


Some other var that may be overrided:
- CMAKE_GENERATOR_TOOLSET
- CMAKE_GENERATOR_PLATFORM

```
cmake -G <generator-name> -T <toolset-spec> -A <platform-name> <path-to-source>
```

## Options for **caching**

We can prepopulate cached information in a CMake script file, that only contains *set()* commands.
```
cmake -C <initial-cache-script> <path-to-source>
```

The initialization and modification of existing cache variables :
```
cmake -D <var>[:<type>]=<value> <path-to-source>
```

<:type> => Optional ( used by GUIs). can be *BOOL, FILEPATH, PATH, STIRNG, or INTERNAL*. If omit, it will be UNINITIALIZED.


One important variable contains the type of the build.

```
cmake -S . -B build -D CMAKE_BUILD_TYPE=Release
```

How to list cache variables : -L option
```
cmake -L[A] [H] <path-to-source>
```

## Options for debugging and tracing

To get general information about variables, commands, macros, and other settings.
```
cmake --system-information [file]
```

Filters the log output of messages based on the current log level
```
cmake --log-levels=<level>
```

```
cmake --log-context <path-to-source>
cmake --trace
```

## Options for presets

```
cmake --list-presets
cmake --preset=<preset>
```

## Executing Script

```
cmake -P script.cmake
```

